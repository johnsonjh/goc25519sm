module github.com/johnsonjh/goc25519sm

go 1.17

require (
	github.com/johnsonjh/leaktestfe v0.0.0-20221210113806-1ad56057a826
	go4.org v0.0.0-20201209231011-d4a079459e60
)

require (
	go.uber.org/goleak v1.2.0 // indirect
	golang.org/x/tools v0.4.1-0.20221213194812-9ec855317b92 // indirect
)
